<?php

namespace App\Services;

use Symfony\Component\DomCrawler\Crawler;

class Parser
{
    private static $link;
    private static $content = [];


    public function __construct($link)
    {
        self::$link = $link;
        self::getPars(null);
    }

    private static function setContent($array)
    {
        self::$content = $array;
    }

    public function getContent(): array
    {
        return self::$content;
    }

    private static function getLink()
    {
        return self::$link;
    }

    private static function crawler($item): Crawler
    {
        return new Crawler(null, $item);
    }

    private static function getPars($links)
    {
        if (isset($links)) {
            foreach ($links as $item) {
                $html = file_get_contents($item);

                $crawler = self::crawler($item);

                $crawler->addHtmlContent($html, 'UTF-8');

                $title = $crawler->filter('h1')->text();

                $body = $crawler->filter('.entry-content')->text();
                $tags = $crawler->filter('.tags-links > a')->each(function (Crawler $node, $i) {
                    return $node->text();
                });

                $image = $crawler->filter('img')->each(function (Crawler $node, $i) {
                    return $node->image()->getUri();
                });


                $content[] = [
                    'title' => $title,
                    'image' => $image[1],
                    'content' => $body,
                    'tags' => $tags,
                    //'body' => $bodies
                ];
                self::setContent($content);
            }
        } else {
            $html = file_get_contents(self::getLink());

            $crawler = self::crawler(self::getLink());
            $crawler->addHtmlContent($html, 'UTF-8');

            $links = $crawler->filter('h2 > a')->each(function (Crawler $node, $i) {
                return $node->link()->getUri();
            });

            self::getPars($links);
        }
    }
}