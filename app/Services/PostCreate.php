<?php

namespace App\Services;

use App\Models\Post;

class PostCreate
{
    private $title;
    private $content;
    private $image;
    private $tags;

    public function __construct($post)
    {
        $this->title = $post['title'];
        $this->content = $post['content'];
        $this->image = $post['image'];
        $this->tags = $post['tags'];
    }

    public function create()
    {
        $post = new Post;
        $post->title = $this->title;
        $post->content = $this->content;
        $post->image = $this->image;
        $post->tags = $this->tags;
        dd($post);
        $post->save();
    }
}