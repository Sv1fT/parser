<?php

namespace App\Http\Controllers;

use App\Services\Parser;
use App\Services\PostCreate;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($page)
    {
        $crawler = new Parser('https://laravel.demiart.ru/'. ($page ? 'page/'.$page.'/': ''));
        foreach ($crawler->getContent() as $post){
            $postCreate = new PostCreate($post);
            $postCreate->create();
        }

    }
}
